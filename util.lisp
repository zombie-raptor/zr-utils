(cl:defpackage #:zr-utils/util
  (:documentation "Basic utilities that do not belong in a more specific file.")
  (:use #:cl
        #:zr-utils/conditions
        #:zr-utils/typed-bindings
        #:zr-utils/types)
  (:import-from #:alexandria
                #:once-only
                #:with-gensyms)
  (:import-from #:closer-mop
                #:compute-applicable-methods-using-classes)
  (:export #:case=
           #:cleanup
           #:define-accessor-macro
           #:define-destructuring-function
           #:define-function
           #:define-simple-slot-saving-for-class
           #:define-simple-struct
           #:define-simple-struct-with-accessor-macro
           #:define-struct-of-arrays
           #:destructuring-lambda
           #:destructuring-lambda*
           #:do-destructuring-bind
           ;; #:do-loop
           ;; #:do-with-bindings
           ;; #:do-with-bindings*
           #:double-float*
           #:ecase=
           #:ensure-the
           #:flet*
           #:float*
           #:implements-cleanup-p
           #:implements-cleanup?
           #:labels*
           #:lambda*
           #:list-if
           #:name
           #:prefix-symbol
           #:read-case
           #:suffix-symbol
           #:surround-symbol
           #:symbol-to-%make-symbol
           #:symbol-to-keyword
           #:symbol-to-make-symbol
           #:with-accessors*
           #:with-bindings
           #:with-bindings*
           #:with-flat-accessors
           #:with-interned-symbols))

(cl:in-package #:zr-utils/util)

(defgeneric cleanup (object)
  (:documentation "
Closes, frees, and/or does other necessary final actions to certain
resources that an object contains.

Ideally, calls to cleanup methods are either contained in an
`unwind-protect' or are called from another `cleanup' method."))

(defgeneric name (object)
  (:documentation "Provides the name of the object, which is probably a keyword."))

(defun implements-cleanup? (instance)
  "Tests to see if an instance of a class has a valid `cleanup' method."
  (compute-applicable-methods-using-classes #'cleanup (list (class-of instance))))

(defun implements-cleanup-p (instance)
  "Tests to see if an instance of a class has a valid `cleanup' method."
  (compute-applicable-methods-using-classes #'cleanup (list (class-of instance))))

;;;; Typed variables

(defmacro define-function (name-and-options typed-lambda-list &body body)
  "
Define a function that is (potentially) 'statically typed' using type
declarations, which have implementation-specific behavior. The types,
which are not required, are provided in a similar way to defmethod: a
list of two items, where the first is the name and the second is the
type.

Keyword and optional arguments can also have a type. When they are
typed, a default value must be provided and the type comes after the
default value. That is, the sublist specifying the typed argument if
it is a keyword or optional is:

  (binding &optional default type)

The provided default value for an optional or keyword argument can be
a value that is not of the required type. In that case, there will be
an error if the caller does not provide a value of the valid type. For
instance, NIL can be used as a default value even if it is not of the
type. This might be desirable if the caller is supposed to provide
their own value. Be careful if you do this because DECLARE is not
required to always error in such situations. This will only lead to
potentially problematic behavior in SBCL with (safety 0).

name-and-options is either a symbol, in which case it is the name, or
it is a list with the CAR being the name and the CDR being an options
plist. The options that can be provided are described below:

:inline tells the compiler that it may inline the function.

:optimize and :debug automatically generate reasonable optimization
declarations for the function if one or both are t.

The :default-type is the type that an argument is assumed to be if no
type is provided. If default-type is NIL then there is no checking for
arguments without a type provided. This is logically equivalent to
providing a default-type of T, but it is potentially more efficient to
use NIL here because then it does not check that the arguments are T.

If :check-type is T, then `check-type' is used instead of type
declarations. Always use `check-type' if you want to be able to fix
the type errors at run time. Never use `check-type' if the types are
only there for efficiency hints in implementations with type
declarations.

Conditionally use `check-type' if the optimal type checking on each
implementation is desired (e.g. check in CLISP and declare in SBCL)
even if that means that the type errors are not fixable at run time in
some implementations (e.g. SBCL with type declarations). This third
option will eventually be the default (i.e. the default value will be
different on different implementations), but currently the default is
type declarations everywhere.

If :return is non-NIL then that is the return type of the function. It
is either a single value or a syntactic (unquoted) list of values. An
ftype is only declared if check-type is NIL and a return type is
provided.

Example usage:

  (define-function foo ((x single-float) (y double-float) &optional (z 42 fixnum))
    (+ x y z))

  (define-function (bar :inline t :return single-float) ((x integer) (y single-float))
    (* x y))
"
  (multiple-value-bind (definition inline ftype)
      (generate-function-definition name-and-options typed-lambda-list body)
    (let ((definition `(defun ,@definition)))
      (if (or inline ftype)
          `(progn (declaim ,@inline ,@ftype)
                  ,definition)
          definition))))

(defmacro flet* (definitions &body body)
  "
Behaves like `flet', but supports the full capabilities of
`define-function' in the function definitions.
"
  (generate-flet*-or-labels* 'flet definitions body))

(defmacro labels* (definitions &body body)
  "
Behaves like `labels', but supports the full capabilities of
`define-function' in the function definitions.
"
  (generate-flet*-or-labels* 'labels definitions body))

(defmacro lambda* (typed-lambda-list &body body)
  "
Supports all of `define-function' that can be supported within a
lambda. At the moment, it only uses the declare style and cannot do
the check-type style.
"
  (multiple-value-bind (lambda-list types)
      (parse-typed-lambda-list typed-lambda-list nil nil)
    `(lambda ,lambda-list
       (declare ,@types)
       ,@body)))

(defmacro destructuring-lambda (lambda-list &body body)
  "Binds a destructuring lambda-list on the sole argument of a lambda"
  (with-gensyms (expression)
    `(lambda (,expression)
       (destructuring-bind ,lambda-list
           ,expression
         ,@body))))

(defmacro destructuring-lambda* ((lambda-list &rest typed-lambda-list) &body body)
  "
Binds a destructuring lambda-list on the first argument and then
provides a lambda*-style lambda-list for the rest.
"
  (with-gensyms (expression)
    `(lambda* ((,expression list) ,@typed-lambda-list)
       (destructuring-bind ,lambda-list
           ,expression
         ,@body))))

(defmacro define-destructuring-function
    (name (destructuring-lambda-list &rest typed-lambda-list) &body body)
  "
Defines a `define-function'-style function whose first argument is a
list with destructuring-bind applied on it and whose remaining
arguments are in a typed-lambda-list.
"
  (with-gensyms (list)
    `(define-function ,name ((,list list) ,@typed-lambda-list)
       (destructuring-bind ,destructuring-lambda-list
           ,list
         ,@body))))

(defmacro do-destructuring-bind ((lambda-list list &optional result) &body body)
  "
Iterates over a list in the style of `dolist' while destructuring it
with a destructuring lambda-list like in `destructuring-bind'.
"
  (with-gensyms (sublist)
    `(dolist (,sublist ,list ,result)
       (destructuring-bind ,lambda-list ,sublist
         ,@body))))

(defmacro with-bindings (bindings &body body)
  "
By default, this macro behaves similarly to `let', but with a type
between the binding and the definition. If the binding begins with
:values, then `multiple-value-bind' is used instead of a single value
bind. If the binding begins with :function or :labels then `flet*' or
`labels*' is used. To keep things simple, labels have access to any
function definition as well as to any label definition.

Support for stack allocation (dynamic-extent declarations), ignored
variables, destructuring binds, and options have not yet been
added. Support for `check-type' instead of type declarations has not
yet been added. When support for these features are added, each
individual binding will be configurable, with an optional :options
line setting the default behavior.
"
  (generate-bindings bindings body 'labels* 'flet* 'let))

(defmacro with-bindings* (bindings &body body)
  "
This is the in-order variation of with-bindings, where each binding or
set of bindings can access the bindings before it.

Note: Consecutive :label bindings are handled as one set of `labels*'
bindings and consecutive let bindings are handled as one set of `let*'
bindings.
"
  (generate-bindings* bindings body 'labels* 'flet* 'let*))

(defmacro ensure-the (type-or-types form)
  "
Behaves like `the', but uses check-type to do the type checking so a
new value can be provided. If not enough return values in form are
given for the types, then the values default to NIL.

Note: This currently does *not* support a custom type that is defined
with values, e.g. (deftype foo () (values integer float))
"
  (car (return-type-check (list form)
                          (if (and (listp type-or-types) (eql (car type-or-types) 'values))
                              (cdr type-or-types)
                              (list type-or-types)))))

;;;; Type coercion

(define-function (float* :inline t) (number)
  "
As a variant of `float', this function always coerces NUMBER to a
single-float instead of only coercing non-floats to single-float.
"
  (coerce number 'single-float))

(define-function (double-float* :inline t) (number)
  "
This is like `float*', but always coerces NUMBER to a double-float
instead of a single-float.
"
  (coerce number 'double-float))

;;;; Symbol functions

(define-function symbol-to-keyword ((symbol symbol))
  "Converts a symbol to a keyword."
  (intern (symbol-name symbol) 'keyword))

(define-function prefix-symbol ((prefix string) (symbol symbol) &optional package)
  "Adds a prefix to a given symbol."
  (let ((symbol* (concatenate 'string prefix (symbol-name symbol))))
    (if package
        (intern symbol* package)
        (intern symbol*))))

(define-function suffix-symbol ((symbol symbol) (suffix string) &optional package)
  "Adds a suffix to a given symbol."
  (let ((symbol* (concatenate 'string (symbol-name symbol) suffix)))
    (if package
        (intern symbol* package)
        (intern symbol*))))

(define-function surround-symbol ((prefix string) (symbol symbol) (suffix string) &optional package)
  "Adds a prefix and a suffix to a given symbol."
  (let ((symbol* (concatenate 'string prefix (symbol-name symbol) suffix)))
    (if package
        (intern symbol* package)
        (intern symbol*))))

(define-function (possibly-surround-symbol :inline t)
    ((prefix symbol) (symbol non-null-symbol) (suffix symbol) &optional package)
  "
Adds a prefix or a suffix to symbol if prefix or suffix are
non-null. Ensures that the result is in the given package.
"
  (cond ((and prefix suffix) (surround-symbol (symbol-name prefix)
                                              symbol
                                              (symbol-name suffix)
                                              package))
        (prefix (prefix-symbol (symbol-name prefix) symbol package))
        (suffix (suffix-symbol symbol (symbol-name suffix) package))
        (t (if package
               (intern (symbol-name symbol) package)
               (intern (symbol-name symbol))))))

(define-function symbol-to-make-symbol ((symbol symbol) &optional package)
  "Adds the prefix MAKE- to a given symbol."
  (prefix-symbol (symbol-name '#:make-) symbol package))

(define-function symbol-to-%make-symbol ((symbol symbol) &optional package)
  "Adds the prefix %MAKE- to a given symbol."
  (prefix-symbol (symbol-name '#:%make-) symbol package))

;;;; Macros

(defmacro list-if (test then)
  `(if ,test (list ,then) nil))

(defmacro with-interned-symbols (symbols &body body)
  (flet ((parse-symbol (symbol)
           (etypecase symbol
             (symbol `(,symbol (intern (symbol-name ',symbol))))
             (list (destructuring-bind (binding symbol &key prefix suffix package)
                       symbol
                     `(,binding (possibly-surround-symbol ',prefix ,symbol ',suffix ,package)))))))
    `(let ,(mapcar #'parse-symbol symbols)
       ,@body)))

(defmacro define-simple-slot-saving-for-class (class)
  "
Tells the CL implementation that it is okay to make a version of the
standard-class or structure-class in a macro and then load that when
calling the function later on. This handles the simple case and is not
designed to handle circular data structures.
"
  `(defmethod make-load-form ((object ,class) &optional environment)
     (make-load-form-saving-slots object :environment environment)))

(defmacro or-with-test ((test form) &rest forms)
  (check-type test symbol)
  (once-only (form)
    `(or ,@(mapcar (lambda (item) `(,test ,item ,form)) forms))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-case= (keyform cases &optional default-error?)
    (flet ((make-case=* (keyform)
             (let* ((default-path? nil)
                    (cases (mapcar (destructuring-lambda (condition &rest implicit-progn)
                                     (typecase condition
                                       (boolean
                                        (when condition
                                          (setf default-path? t))
                                        `(,condition ,@implicit-progn))
                                       (list
                                        `((or-with-test (= ,keyform) ,@condition) ,@implicit-progn))
                                       (t
                                        `((= ,condition ,keyform) ,@implicit-progn))))
                                   cases)))
               `(cond ,@(if (and default-error? (not default-path?))
                            (append cases `((t (error "~A fell through ecase= expression." ,keyform))))
                            cases)))))
      (destructuring-bind (binding tail)
          (cdr (once-only (keyform)
                 (list `(declare (ignorable ,keyform))
                       (make-case=* keyform))))
        `(let ,binding ,@tail)))))

(defmacro case= (keyform &body cases)
  "Behaves like `case', but tests with `='."
  (make-case= keyform cases))

(defmacro ecase= (keyform &body cases)
  "Behaves like `ecase', but tests with `='."
  (make-case= keyform cases t))

(defmacro define-simple-struct (name &body names-and-types)
  "
Defines a struct using a simplified slot syntax where the default
value is determined by the type of the slot. If a symbol is given
instead of a list, then this macro assumes that the name and the type
for the slot are the same.
"
  (labels ((struct-default (type options)
             (etypecase type
               (symbol (case type
                         (boolean (getf options :initial-value t))
                         ((fixnum
                           int4 int8 int16 int32 int64
                           uint4 uint8 uint16 uint32 uint64)
                          (getf options :initial-value 0))
                         (t `(,(symbol-to-make-symbol type) ,@options))))
               (list (destructuring-bind (name &rest arguments)
                         type
                       (case name
                         (mod (getf options :initial-value 0))
                         (simple-array (destructuring-bind (element-type dimensions)
                                           arguments
                                         `(make-array ,@(if (= 1 (length dimensions))
                                                            (list (first dimensions))
                                                            (list dimensions))
                                                      :element-type ',element-type
                                                      ,@(if (member :initial-element options)
                                                            nil
                                                            `(:initial-element ,(cond ((subtypep element-type 'number) (coerce 0 element-type))
                                                                                      ((subtypep element-type 'character) (code-char 0))
                                                                                      (t nil))))
                                                      ,@options)))
                         (simple-bit-vector (destructuring-bind (size) arguments
                                              `(make-array ,size
                                                           :element-type 'bit
                                                           :initial-element ,(getf options :initial-element 0))))
                         (t `(,(symbol-to-make-symbol name) ,@arguments ,@options)))))))
           (parse-slot (name-and-type)
             (etypecase name-and-type
               (list (destructuring-bind (name type &rest options)
                         name-and-type
                       `(,name ,(struct-default type options) :type ,type)))
               (symbol `(,name-and-type ,(struct-default name-and-type '())
                                        :type ,name-and-type)))))
    `(defstruct ,name
       ,@(mapcar #'parse-slot names-and-types))))

(defmacro define-simple-struct-with-accessor-macro (name &body name-and-types)
  "
This macro is like `define-simple-struct', but it also generates a
`define-accessor-macro'.
"
  (let* ((name* (if (listp name) (car name) name))
         (accessor-name (possibly-surround-symbol '#:with- name* '#:-accessors))
         (prefix (concatenate 'string (symbol-name name*) "-")))
    `(progn (define-simple-struct ,name ,@name-and-types)
            (define-accessor-macro ,accessor-name ,prefix))))

(defmacro define-struct-of-arrays (name size &body names-and-types)
  `(defstruct ,name
     ,@(mapcar (destructuring-lambda (name type &rest options)
                 (let ((second-size (getf options :size)))
                   (remf options :size)
                   `(,name (make-array '(,size ,@(if second-size (list second-size)))
                                       :element-type ',type
                                       ,@options)
                           :type (simple-array ,type ,(append (list size) (if second-size (list second-size)))))))
               names-and-types)))

(defmacro with-accessors* (slots instance &body body)
  "
Binds symbols to accessors like in `with-accessors', but it also
permits symbols in the SLOTS list instead of a list of two symbols. If
this is the case, then it is assumed that the accessor and local
binding names are identical, which saves quite a bit of boilerplate.
An example that mixes the two:

  (with-accessors* ((count foo-count)
                    items)
      object
    (dotimes (i count)
      (example-function items)))

In this example, the accessor foo-count is bound to count, but the
entry for items is automatically turned into (items items), which
binds the accessor items to items. Since most accessor names are very
generic, this latter case is a lot more common than the former, making
this macro one of the most useful ones in this library for boilerplate
reduction.
"
  (let ((slots* (mapcar (lambda (slot)
                          (etypecase slot
                            (list (destructuring-bind (binding accessor)
                                      slot
                                    (check-type binding symbol)
                                    (check-type accessor symbol)
                                    slot))
                            (symbol `(,slot ,slot))))
                        slots)))
    `(with-accessors ,slots* ,instance
       ,@body)))

(defmacro define-accessor-macro (name &optional (prefix "") (package *package*))
  (flet ((prefixed-accessor (prefix)
           (let ((symbol (if (equalp prefix "")
                             `(intern (symbol-name accessor) ,package)
                             `(prefix-symbol ,prefix accessor ,package))))
             `(lambda (slot)
                (multiple-value-bind (binding accessor)
                    (etypecase slot
                      (list (destructuring-bind (binding accessor) slot
                              (check-type binding symbol)
                              (check-type accessor symbol)
                              (values binding accessor)))
                      (symbol (values slot slot)))
                  `(,binding ,,symbol))))))
    (with-interned-symbols (slots slots* instance body)
      `(defmacro ,name (,slots ,instance &body ,body)
         (let ((,slots* (mapcar ,(prefixed-accessor (etypecase prefix
                                                      (string prefix)
                                                      (null "")
                                                      (symbol (symbol-name prefix))))
                                ,slots)))
           `(with-accessors ,,slots* ,,instance
              ,@,body))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun unflatten-accessors (with-object-accessors body)
    (destructuring-bind ((macro slots instance) &rest accessor-macros)
        with-object-accessors
      (if accessor-macros
          (list macro slots instance (unflatten-accessors accessor-macros body))
          (list* macro slots instance body)))))

(defmacro with-flat-accessors (with-object-accessors &body body)
  (if with-object-accessors
      (unflatten-accessors with-object-accessors body)
      `(progn ,@body)))
