zr-utils
========

A Common Lisp utilities library originating from the Zombie Raptor
game engine project.

The main macro in this repository is the `define-function` macro,
which allows for a more convenient syntax for defining functions,
especially with `declare`d types. This is a fairly easy macro to
write, but a hard macro to write comprehensively because of quite a
few edge cases. Unlike many other versions of this macro,
`define-function` aims to address *all* of these cases. This can be
seen by the very large number of features it supports.
