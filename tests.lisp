(uiop:define-package #:zr-utils/tests
  (:use #:cl)
  (:use-reexport #:zr-utils/tests/all
                 #:zr-utils/tests/reader-patterns))
