(cl:defpackage #:zr-utils/conditions
  (:documentation "Some common conditions.")
  (:use #:cl)
  (:export #:container
           #:details
           #:empty-container-error
           #:error-unless
           #:error-when
           #:macro-syntax-error
           #:missing-input-name
           #:missing-object-error
           #:object-name
           #:printable-class-name
           #:printable-type-name
           #:required-input-error))

(cl:in-package #:zr-utils/conditions)

;;; Macros

(defmacro error-when (test datum &rest arguments)
  `(when ,test (error ,datum ,@arguments)))

(defmacro error-unless (test datum &rest arguments)
  `(unless ,test (error ,datum ,@arguments)))

;;; Conditions

(define-condition macro-syntax-error (error)
  ((%details
    :initarg :details
    :reader details))
  (:report (lambda (condition stream)
             (format stream "Invalid macro syntax")
             (when (slot-boundp condition '%details)
               (format stream ": ~A" (details condition)))))
  (:documentation "An error for when a macro is used with invalid syntax."))

(define-condition required-input-error (error)
  ((%printable-class-name
    :initarg :printable-class-name
    :reader printable-class-name)
   (%missing-input-name
    :initarg :missing-input-name
    :reader missing-input-name))
  (:report (lambda (condition stream)
             (let ((class-bound? (slot-boundp condition '%printable-class-name))
                   (input-bound? (slot-boundp condition '%missing-input-name)))
               (cond ((and class-bound? input-bound?)
                      (format stream
                              "~A requires ~A"
                              (printable-class-name condition)
                              (missing-input-name condition)))
                     (class-bound?
                      (format stream
                              "~A is missing a required input"
                              (printable-class-name condition)))
                     (input-bound?
                      (format stream
                              "Required input (~A) must be provided"
                              (missing-input-name condition)))
                     ((and (not class-bound?) (not input-bound?))
                      (format stream "A required input was not provided"))))))
  (:documentation "
An error for when a keyword argument, such as a keyword of a
make-instance, is not provided but is required.
"))

(define-condition missing-object-error (error)
  ((%printable-type-name
    :initarg :printable-type-name
    :reader printable-type-name)
   (%object-name
    :initarg :object-name
    :reader object-name)
   (%details
    :initarg :details
    :reader details
    :initform nil))
  (:report (lambda (condition stream)
             (format stream
                     "~:[An~;The ~:*~A~] object ~@[with the name ~A ~]is missing."
                     (if (slot-boundp condition '%printable-type-name)
                         (printable-type-name condition)
                         nil)
                     (if (slot-boundp condition '%object-name)
                         (object-name condition)
                         nil))
             (when (details condition)
               (format stream " ~A" (details condition)))))
  (:documentation "
An error for when an object is expected, but no object was provided.
This could be due to a missing entry in a hash-table, in which case it
will have an object-name, which is probably the hash-table key.
"))

(define-condition empty-container-error (error)
  ((%container
    :initarg :container
    :reader container)
   (%details
    :initarg :details
    :reader details
    :initform nil))
  (:report (lambda (condition stream)
             (if (slot-boundp condition '%container)
                 (format stream
                         "The container ~S is empty."
                         (container condition))
                 (format stream
                         "A container is empty that should have contents."))
             (when (details condition)
               (format stream " ~A" (details condition)))))
  (:documentation "
An error for when some container, such as a sequence or hash-table, is
empty, but at least one item is needed.
"))
