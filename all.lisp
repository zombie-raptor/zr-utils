(uiop:define-package #:zr-utils/all
  (:nicknames #:zr-utils)
  (:use #:cl
        #:zr-utils/reader-patterns
        #:zr-utils/typed-bindings)
  (:use-reexport #:zr-utils/array
                 #:zr-utils/conditions
                 #:zr-utils/data-structures
                 #:zr-utils/hash-table
                 #:zr-utils/metaobject
                 #:zr-utils/modify-macros
                 #:zr-utils/read
                 #:zr-utils/stream
                 #:zr-utils/strings
                 #:zr-utils/types
                 #:zr-utils/util))
