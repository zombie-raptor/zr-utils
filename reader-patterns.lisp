;;;; This is an internal file that deals with the complicated patterns
;;;; used in READ-CASE and similar macros that are designed to turn
;;;; patterns on character streams into a format that Lisp can
;;;; understand.
;;;;
;;;; Note: This file is "internal". You probably do not need to use
;;;; what it exports.

(cl:defpackage #:zr-utils/reader-patterns
  (:documentation "Transforms character patterns for character reader macros.")
  (:use #:cl
        #:zr-utils/conditions
        #:zr-utils/hash-table
        #:zr-utils/metaobject
        #:zr-utils/util)
  (:export #:category
           #:end
           #:generate-reader-matches
           #:pattern-equal?
           #:range
           #:start
           #:symbol-pattern
           #:unicode-category))

(cl:in-package #:zr-utils/reader-patterns)

;;;; Unicode

(deftype unicode-general-categories ()
  '(and keyword
    (member
     :lu :ll :lt :lm :lo
     :mn :mc :me
     :nd :nl :no
     :pc :pd :ps :pe :pi :pf :po
     :sm :sc :sk :so
     :zs :zl :zp
     :cc :cf :cs :co :cn)))

(define-function (in-unicode-category? :inline t) ((character character) (category keyword))
  "
Determines if a character is in the given Unicode general category. If
there is an implementation-specific fast way to test this, that test
is used. Otherwise, it uses a slower, portable test.

Note: This doesn't check if category is valid because that is a much
slower test than just checking if it is a keyword.
"
  #-sb-unicode
  (eql (nth-value 1 (cl-unicode:general-category character))
       (intern (symbol-name category) :cl-unicode-names))
  #+sb-unicode
  (eql (sb-unicode:general-category character) category))

;;;; Reader patterns

(defgeneric pattern-to-test (pattern character-binding)
  (:documentation "Turns a pattern into Lisp source code testing for that pattern."))

(defgeneric insert-pattern (pattern patterns)
  (:documentation "Inserts a pattern into a list of patterns."))

(defclass reader-pattern ()
  ((%pattern
    :initarg :pattern
    :reader pattern
    :initform 0
    :checked-type (integer 0 *)))
  (:metaclass checked-types))

(defclass range (reader-pattern)
  ((%start
    :initarg :start
    :accessor %start
    :reader start
    :initform (error "A ~A needs a start" 'range)
    :checked-type character)
   (%end
    :initarg :end
    :accessor %end
    :reader end
    :initform nil
    :checked-type (or null character)))
  (:metaclass checked-types))

(defclass unicode-category (reader-pattern)
  ((%category
    :initarg :category
    :reader category
    :initform (error "A ~A needs a category" 'unicode-category)
    :checked-type unicode-general-categories))
  (:metaclass checked-types))

(defclass symbol-pattern (reader-pattern)
  ((%symbol
    :initarg :symbol
    :reader symbol-pattern
    :initform nil
    :checked-type (or null (eql t) (eql :eof))))
  (:metaclass checked-types))

;;; Verifies that the character range is a valid range, i.e.
;;; consisting of one or two characters, where the end comes after the
;;; start if the end exists.
(defmethod initialize-instance :after ((range range)
                                       &key
                                         start
                                         end
                                       &allow-other-keys)
  (cond ((and end (char= start end))
         (setf (%end range) nil))
        ((and end (char< end start))
         (error "Error in creating a ~A: ~A cannot come before ~A"
                'range
                (start range)
                (end range)))))

;;; Prints a character range, consisting of one or two characters.
(defmethod print-object ((range range) stream)
  (print-unreadable-object (range stream :type t)
    (format stream
            "~A~@[ ~A~]"
            (start range)
            (end range))))

(defmethod print-object ((category unicode-category) stream)
  (print-unreadable-object (category stream :type t)
    (format stream
            "~S"
            (category category))))

(defmethod print-object ((p symbol-pattern) stream)
  (print-unreadable-object (p stream :type t)
    (format stream
            "~S"
            (symbol-pattern p))))

(define-function (range :inline t) (start &optional end (pattern 0))
  "Syntactic sugar for creating a range object."
  (make-instance 'range :start start :end end :pattern pattern))

;;; TODO: allow negation of ranges, which either char/'s here if one
;;; character or turns the range into its complement.
(defmethod pattern-to-test ((range range) (character-binding symbol))
  `(,(if (end range)
         `(char<= ,(start range) ,character-binding ,(end range))
         `(char= ,(start range) ,character-binding))
    ,(pattern range)))

(defun pattern-to-test* (symbol character-binding pattern-number)
  (etypecase symbol
    ((eql t) `(t ,pattern-number))
    (null `((null ,character-binding) ,pattern-number))
    ((eql :eof) `((eql ,character-binding ,symbol)
                            ,pattern-number))))

(defmethod pattern-to-test ((symbol-pattern symbol-pattern) (character-binding symbol))
  (pattern-to-test* (symbol-pattern symbol-pattern)
                    character-binding
                    (pattern symbol-pattern)))

(defmethod pattern-to-test ((c unicode-category) (character-binding symbol))
  `((in-unicode-category? ,character-binding ,(category c))
    ,(pattern c)))

(defmethod pattern-to-test ((l list) (s symbol))
  (loop :for p :in l
        :collect (pattern-to-test p s)))

;;;; Generates reader matches

(defun make-pattern (pattern pattern-match)
  (etypecase pattern
    ((or null (eql t) (eql :eof))
     (make-instance 'symbol-pattern
                    :symbol pattern
                    :pattern pattern-match))
    (character
     (make-instance 'range
                    :start pattern
                    :pattern pattern-match))
    (list
     (destructuring-bind (command &rest args) pattern
       (ecase command
         (:or (destructuring-bind (&rest patterns) args
                (loop :for pattern :in patterns
                      :collect (make-pattern pattern pattern-match))))
         (:range (destructuring-bind (start &optional end) args
                   (make-instance 'range
                                  :start start
                                  :end end
                                  :pattern pattern-match))))))
    (unicode-general-categories
     (make-instance 'unicode-category
                    :category pattern
                    :pattern pattern-match))))

;;; Generates the body for a pattern that must come first in the
;;; pattern tests, i.e. a non-character that can result from
;;; `read-char' or `read-char-no-hang'.
(defun %early-pattern (pattern special-patterns character)
  (cond ((getf special-patterns pattern)
         `(,(pattern-to-test (getf special-patterns pattern)
                             character)))
        ((getf special-patterns t)
         `(,(pattern-to-test* pattern
                              character
                              (pattern (getf special-patterns t)))))
        (t
         `(,(pattern-to-test* pattern character nil)))))

;;; TODO: restore :not which needs to be able to negate ranges
(defun generate-reader-matches (cases character no-hang?)
  ;; Enumerates each case into a pattern number.
  (loop :for case :in cases
        :for test := (car case)
        :for pattern-match :from 0
        :for pattern := (make-pattern test pattern-match)
        :with special-patterns := nil
        :if (typep pattern 'symbol-pattern)
          :do (cond ((getf special-patterns test)
                     (warn "Duplicate test for ~S is unreachable" (symbol-pattern pattern)))
                    ((and (null test) (not no-hang?))
                     (warn "NIL can only be a pattern when using a no-hang read. The EOF pattern is always :EOF instead of NIL."))
                    (t
                     (setf (getf special-patterns test)
                           pattern)))
        :else
          :if (typep pattern 'list)
            :append (loop :for p :in pattern
                          :if (typep p 'symbol-pattern)
                            :do (if (getf special-patterns (symbol-pattern p))
                                    (warn "Duplicate test for ~S is unreachable" (symbol-pattern p))
                                    (setf (getf special-patterns (symbol-pattern p))
                                          p))
                          :else
                            :collect p)
              :into patterns
        :else
          :collect pattern :into patterns
        :finally (return
                   `(cond
                      ;; These have to be tested first because these
                      ;; aren't characters and the later tests assume
                      ;; characters.
                      ,@(if no-hang?
                            (%early-pattern nil special-patterns character)
                            nil)
                      ,@(%early-pattern :eof special-patterns character)
                      ,@(loop :for p :in patterns
                              :if (listp p)
                                :append (pattern-to-test p character)
                              :else
                                :collect (pattern-to-test p character))
                      ;; This has to come at the end because it
                      ;; matches everything.
                      ,@(if special-patterns
                            `(,@(if (getf special-patterns t)
                                    `(,(pattern-to-test (getf special-patterns t)
                                                        character))))
                            nil)))))
