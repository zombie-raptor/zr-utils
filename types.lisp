(cl:defpackage #:zr-utils/types
  (:documentation "Commonly used type definitions.")
  (:use #:cl)
  (:export #:int4
           #:int8
           #:int16
           #:int32
           #:int64
           #:maybe
           #:non-null-symbol
           #:octet
           #:uint4
           #:uint8
           #:uint16
           #:uint32
           #:uint64))

(cl:in-package #:zr-utils/types)

;;; Mostly used as the type of all of those default values of
;;; optional/keyword arguments or empty slots that default to NIL
(deftype maybe (type)
  `(or null ,type))

(deftype non-null-symbol ()
  `(and symbol (not null)))

;;; Common abbreviation for an 8-bit byte
(deftype octet () `(unsigned-byte 8))

;;; Types that match the size-based C (CFFI) names, for code that's
;;; using CFFI

(deftype int4  () `(signed-byte 4))
(deftype int8  () `(signed-byte 8))
(deftype int16 () `(signed-byte 16))
(deftype int32 () `(signed-byte 32))
(deftype int64 () `(signed-byte 64))

(deftype uint4  () `(unsigned-byte 4))
(deftype uint8  () `(unsigned-byte 8))
(deftype uint16 () `(unsigned-byte 16))
(deftype uint32 () `(unsigned-byte 32))
(deftype uint64 () `(unsigned-byte 64))
