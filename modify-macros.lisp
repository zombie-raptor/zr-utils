(cl:defpackage #:zr-utils/modify-macros
  (:documentation "Modify macros and their associated functions")
  (:use #:cl
        #:zr-utils/util)
  (:export #:decf-mod
           #:divf
           #:exptf
           #:incf-mod
           #:mod+
           #:mod-
           #:modf
           #:multf))

(cl:in-package #:zr-utils/modify-macros)

(define-function (mod+ :inline t) (number divisor &optional (delta 1))
  "
Increments NUMBER by DELTA and then takes the `mod' of the result with
DIVISOR. The order might seem counter-intuitive, but that's because
this function is designed to be used with `incf-mod'.
"
  (mod (+ number delta) divisor))

(define-function (mod- :inline t) (number divisor &optional (delta 1))
"
Decrements NUMBER by DELTA and then takes the `mod' of the result with
DIVISOR. The order might seem counter-intuitive, but that's because
this function is designed to be used with `decf-mod'.
"
  (mod (- number delta) divisor))

(define-modify-macro incf-mod (divisor &optional (delta 1)) mod+
  "Increments a number like `incf', but wraps the result with `mod'")

(define-modify-macro decf-mod (divisor &optional (delta 1)) mod-
  "Decrements a number like `decf', but wraps the result with `mod'")

(define-modify-macro multf (&optional (multiplicand 2)) *
  "Behaves like `incf', except with multiplication; defaults to doubling.")

(define-modify-macro divf (&optional (dividend 2)) /
  "Behaves like `decf', except with division; defaults to halving.")

(define-modify-macro exptf (&optional (power 2)) expt
  "Behaves like `incf', except with `expt'; defaults to squaring.")

(define-modify-macro modf (&optional (divisor 2)) mod
  "Modifies a place with its value modulo a divisor, defaulting to mod 2.")
