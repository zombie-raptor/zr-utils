(cl:defpackage #:zr-utils/metaobject
  (:documentation "Defines metaclasses. This currently defines one metaclass, which guarantees type checking on all slot accesses.")
  (:use #:cl)
  (:import-from #:closer-mop
                #:compute-effective-slot-definition
                #:direct-slot-definition-class
                #:effective-slot-definition-class
                #:slot-value-using-class
                #:standard-direct-slot-definition
                #:standard-effective-slot-definition
                #:standard-slot-definition
                #:validate-superclass)
  (:export #:checked-types))

(cl:in-package #:zr-utils/metaobject)

(defclass checked-types (standard-class)
  ()
  (:documentation "
Ensures that types defined in a slot's :checked-type are always
checked."))

(defclass slot-with-checked-type (standard-slot-definition)
  ((checked-type
    :initarg :checked-type
    :reader slot-definition-checked-type
    :documentation "
Provides a type that will be checked in the setters and
constructor. Unlike :type, :checked-type is guaranteed to be checked
in all implementations and all optimization levels."))
  (:documentation "
Slots that provide a type that is guaranteed to be checked at
runtime."))

(defclass direct-slot-with-checked-type (standard-direct-slot-definition slot-with-checked-type) ())

(defclass effective-slot-with-checked-type (standard-effective-slot-definition slot-with-checked-type) ())

(defmethod validate-superclass ((class checked-types) (super-class standard-class))
  t)

(defmethod direct-slot-definition-class ((class checked-types) &key checked-type &allow-other-keys)
  (if checked-type
      (find-class 'direct-slot-with-checked-type)
      (call-next-method)))

(defmethod effective-slot-definition-class ((class checked-types) &key &allow-other-keys)
  (find-class 'effective-slot-with-checked-type))

;;; Note: SBCL hardcodes the initargs to the method
;;; `effective-slot-definition-class' so it cannot be used like
;;; `direct-slot-definition-class'.
(defmethod compute-effective-slot-definition :around ((class checked-types) name direct-slot-definitions)
  (declare (ignore name))
  (destructuring-bind (slot)
      direct-slot-definitions
    (let ((intermediate-result (call-next-method)))
      (setf (slot-value intermediate-result 'checked-type)
            (if (typep slot 'direct-slot-with-checked-type)
                (slot-definition-checked-type slot)
                nil))
      intermediate-result)))

(defmethod (setf slot-value-using-class) :before (new-value (class checked-types) object (slot effective-slot-with-checked-type))
  (declare (ignore object))
  (unless (null (slot-definition-checked-type slot))
    (assert (typep new-value (slot-definition-checked-type slot))
            (new-value)
            "~S is not of type ~S"
            new-value
            (slot-definition-checked-type slot))))
