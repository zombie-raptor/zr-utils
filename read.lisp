(cl:defpackage #:zr-utils/read
  (:documentation "Functions and macros for variations on READ-")
  (:use #:cl
        #:zr-utils/reader-patterns
        #:zr-utils/util)
  (:import-from #:alexandria
                #:once-only
                #:with-gensyms)
  (:export #:read-case))

(cl:in-package #:zr-utils/read)

(defmacro read-case ((stream match &key no-hang?) &body cases)
  "
Reads from the stream character by character and attempts to match
that character.

TODO: The below has not been completely implemented yet.

The pattern matching language is described below:

 - There are several special symbols. T represents the default match.
   It will only be taken if there is no other match. NIL is returned
   if no-hang? and the stream currently has nothing available. :EOF
   represents the end of file.

 - A literal character represents any literal character match.

 - :OR represent more than one pattern leading to the same next state.

 - :UNION is a restricted :OR representing a set containing the union
   of its contents. It can only contain characters and ranges, but the
   benefit of this is that :NOT can be applied to it, representing the
   set complement. This should roughly be equivalent to a regular
   expression's [...] and [^...] respectively.

 - :RANGE represents a sequential range of characters, or its
   complement if it is contained within either a :NOT or a :UNION that
   is contained within a :NOT. For portability, implementations
   without Unicode `char-code's should have a portability layer
   applied, hurting performance, but leading to portable pattern
   matching. This has not yet been implemented.
"
  (once-only (stream)
    (with-gensyms (branch)
      (let ((read-char (if no-hang? 'read-char-no-hang 'read-char)))
        `(let* ((,match (,read-char ,stream nil :eof t))
                (,branch ,(generate-reader-matches cases match no-hang?)))
           (when ,branch
             (case= ,branch
               ,@(loop :for i :from 0
                       :for case :in cases
                       :collect `(,i ,@(rest case))))))))))
