This project has been split off from the Zombie Raptor game engine
project, so see [its contributing
guide](https://gitlab.com/zombie-raptor/zombie-raptor/-/blob/master/CONTRIBUTING.md)
for now.
