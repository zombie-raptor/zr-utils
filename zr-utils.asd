;;; Requires an ASDF version with package-inferred-system
(cl:unless (asdf:version-satisfies (asdf:asdf-version) "3.1.2")
  (cl:error "zr-utils requires ASDF 3.1.2 or later."))

(asdf:defsystem #:zr-utils
  :description "
A Common Lisp utilities library originating from the Zombie Raptor
game engine project.
"
  :version "0.0.0.0"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :homepage "https://gitlab.com/zombie-raptor/zr-utils"
  :bug-tracker "https://gitlab.com/zombie-raptor/zr-utils/issues"
  :source-control (:git "https://gitlab.com/zombie-raptor/zr-utils.git")
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :in-order-to ((asdf:test-op (asdf:test-op :zr-utils/tests)))
  :depends-on (:alexandria
               #-sb-unicode :cl-unicode
               #-sbcl :babel
               :closer-mop
               :trivial-gray-streams
               :trivial-with-current-source-form
               :uiop
               :zr-utils/all))

(asdf:defsystem #:zr-utils/tests
  :description "The tests for ZR Utils."
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :depends-on (:fiveam)
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :perform (asdf:test-op (op s) (uiop:symbol-call :fiveam
                                                  :run!
                                                  (cl:intern (cl:symbol-name '#:zr-utils/tests)
                                                             '#:zr-utils/tests))))

(asdf:register-system-packages :fiveam :it.bese.fiveam)
