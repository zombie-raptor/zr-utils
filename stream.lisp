(cl:defpackage #:zr-utils/stream
  (:documentation "Defines several useful custom streams.")
  (:use #:cl
        #:trivial-gray-streams
        #:zr-utils/array
        #:zr-utils/data-structures
        #:zr-utils/modify-macros
        #:zr-utils/types
        #:zr-utils/util)
  (:import-from #:alexandria)
  (:export #:byte-pipe
           #:character-pipe
           #:empty?))

(cl:in-package #:zr-utils/stream)

;;; The generic function EMPTY? is used to test if the custom stream
;;; types defined in this file are empty.
;;;
;;; This function uses Alexandria's EMPTYP for sequences and its own
;;; trivial test for hash tables. It doesn't provide an alternate
;;; EMPTYP or EMPTY-P name because that would be confused with
;;; Alexandria's version even though this is more general.
(defgeneric empty? (object))

(defmethod empty? ((sequence sequence))
  (alexandria:emptyp sequence))

(defmethod empty? ((hash-table hash-table))
  (zerop (hash-table-count hash-table)))

;;;; Common buffer definitions

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +buffer-size+ (expt 2 13)))

;;;; Typed buffers (byte and character)

(define-struct-queue byte-buffer ((byte octet) +buffer-size+))
(define-struct-queue character-buffer ((character character) +buffer-size+))

;;;; Byte Pipe

(defclass byte-pipe (fundamental-binary-output-stream
                     fundamental-binary-input-stream)
  ((%buffer
    :initform (make-byte-buffer)
    :reader buffer)))

(defmethod stream-write-byte ((stream byte-pipe) byte)
  (byte-buffer-enqueue (buffer stream) byte))

(defmethod stream-read-byte ((stream byte-pipe))
  (byte-buffer-dequeue (buffer stream)))

(defmethod empty? ((stream byte-pipe))
  (byte-buffer-empty? (buffer stream)))

(defmethod stream-element-type ((stream byte-pipe))
  '(unsigned-byte 8))

;;;; Character Pipe

(defclass character-pipe (fundamental-character-output-stream
                          fundamental-character-input-stream)
  ((%buffer
    :initform (make-character-buffer)
    :reader buffer)))

(defmethod stream-write-char ((stream character-pipe) (character character))
  (character-buffer-enqueue (buffer stream) character))

(defmethod stream-read-char ((stream character-pipe))
  (let ((buffer (buffer stream)))
    (if (character-buffer-empty? buffer)
        :eof
        (character-buffer-dequeue buffer))))

(defmethod stream-unread-char ((stream character-pipe) character)
  (declare (ignore character))
  (let ((buffer (buffer stream)))
    (decf-mod (character-buffer-start buffer) +buffer-size+))
  nil)

(defmethod empty? ((stream character-pipe))
  (character-buffer-empty? (buffer stream)))
