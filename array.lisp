(cl:defpackage #:zr-utils/array
  (:documentation "Helper functions and macros to make working with 1D and 2D arrays easier.")
  (:use #:cl
        #:zr-utils/conditions
        #:zr-utils/hash-table
        #:zr-utils/types
        #:zr-utils/util)
  (:import-from #:alexandria
                #:with-gensyms)
  (:export #:array-of-2
           #:array-of-3
           #:array-of-4
           #:array-row-of-2
           #:array-row-of-3
           #:array-row-of-4
           #:define-array-and-type
           #:flat-index
           #:with-array-accessors))

(cl:in-package #:zr-utils/array)

;;;; Functions

(define-function (flat-index :inline t) (stride row-size i j)
  "
Returns the one-dimensional index of a one-dimensional array that
pretends to be two-dimensional with a \"stride\" (i.e. data can be
larger than one index).
"
  (* stride (+ i (* row-size j))))

(define-function (array-of-2 :inline t) (array &optional (offset 0))
  "Returns the first two values of an array."
  (values (aref array (+ offset 0))
          (aref array (+ offset 1))))

(define-function (array-of-3 :inline t) (array &optional (offset 0))
  "Returns the first three values of an array."
  (values (aref array (+ offset 0))
          (aref array (+ offset 1))
          (aref array (+ offset 2))))

(define-function (array-of-4 :inline t) (array &optional (offset 0))
  "Returns the first four values of an array."
  (values (aref array (+ offset 0))
          (aref array (+ offset 1))
          (aref array (+ offset 2))
          (aref array (+ offset 3))))

(define-function (array-row-of-2 :inline t) (array row &optional (offset 0))
  "Returns the first two values of an array row."
  (values (aref array row (+ offset 0))
          (aref array row (+ offset 1))))

(define-function (array-row-of-3 :inline t) (array row &optional (offset 0))
  "Returns the first three values of an array row."
  (values (aref array row (+ offset 0))
          (aref array row (+ offset 1))
          (aref array row (+ offset 2))))

(define-function (array-row-of-4 :inline t) (array row &optional (offset 0))
  "Returns the first four values of an array row."
  (values (aref array row (+ offset 0))
          (aref array row (+ offset 1))
          (aref array row (+ offset 2))
          (aref array row (+ offset 3))))

;;;; Macros

(defmacro define-array-and-type (name (type size &key (initial-element 0) (arguments nil)) &optional contents-name-list)
  "
Defines a type and constructor for an array with the specified element
type, size, and initial element. By default, only one constructor
function is given, as make-name. If the contents are given a name,
then a second constructor function is created, with just the type name
as the function name. This second constructor just takes in the
elements in order, like with `vector' or `list'.
"
  `(progn (deftype ,name ,arguments
            `(simple-array ,',type ,,size))
          ;; The regular constructor, a limited form of `make-array'
          (define-function (,(symbol-to-make-symbol name) :inline t)
              (,@arguments &key (initial-element ,(coerce initial-element `,type)))
            (make-array ,size
                        :element-type ',type
                        :initial-element initial-element))
          ;; The optional second constructor, a finite-length function
          ;; in the style of `vector'
          ,(when contents-name-list
             `(define-function (,name :inline t) ,contents-name-list
                (make-array ,size
                            :element-type ',type
                            :initial-contents (list ,@contents-name-list))))))

(defmacro set-array (array variable-list &optional (offset 0))
  "
Replaces the first N values of an array, where N is the length of
variable-list.
"
  `(progn (psetf ,@(loop :for variable :in variable-list
                         :for i :from 0
                         :collect `(aref ,array (+ ,offset ,i))
                         :collect variable))
          ,array))

(defmacro set-array-row (array row variable-list &optional (offset 0))
  "
Replaces the first N values of an array row, where N is the length of
variable-list.
"
  `(progn (psetf ,@(loop :for variable :in variable-list
                         :for i :from 0
                         :collect `(aref ,array ,row (+ ,offset ,i))
                         :collect variable))
          ,array))

(defsetf array-of-2 (array &optional (offset 0)) (variable-0 variable-1)
  "Sets the first two values of an array."
  `(set-array ,array (,variable-0 ,variable-1) ,offset))

(defsetf array-of-3 (array &optional (offset 0)) (variable-0 variable-1 variable-2)
  "Sets the first three values of an array."
  `(set-array ,array (,variable-0 ,variable-1 ,variable-2) ,offset))

(defsetf array-of-4 (array &optional (offset 0)) (variable-0 variable-1 variable-2 variable-3)
  "Sets the first four values of an array."
  `(set-array ,array (,variable-0 ,variable-1 ,variable-2 ,variable-3) ,offset))

(defsetf array-row-of-2 (array row &optional (offset 0)) (variable-0 variable-1)
  "Sets the first two values of an array row."
  `(set-array-row ,array ,row (,variable-0 ,variable-1) ,offset))

(defsetf array-row-of-3 (array row &optional (offset 0)) (variable-0 variable-1 variable-2)
  "Sets the first three values of an array row."
  `(set-array-row ,array ,row (,variable-0 ,variable-1 ,variable-2) ,offset))

(defsetf array-row-of-4 (array row &optional (offset 0)) (variable-0 variable-1 variable-2 variable-3)
  "Sets the first four values of an array row."
  `(set-array-row ,array ,row (,variable-0 ,variable-1 ,variable-2 ,variable-3) ,offset))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun row-of-and-elt-error-details ()
    (format nil
            " when defining a 2D array slot:~% A binding cannot have both an ~S and a ~S~%"
            :elt
            :row-of))

  (defun parse-array-accessor (first-index slot slot-gensyms)
    (if (listp slot)
        (destructuring-bind (binding slot-name &key row-of range elt = offset stride)
            slot
          (check-type row-of (maybe (integer 1 4)))
          (check-type range (maybe (integer 1 4)))
          (error-when (or (and row-of (or range elt)) (and range elt))
                      'macro-syntax-error
                      :details (row-of-and-elt-error-details))
          (let ((binding (if (string= (symbol-name binding) "_")
                             (gensym (symbol-name slot-name))
                             binding))
                (slot-name (gethash slot-name slot-gensyms))
                (first-index (if stride
                                 `(* ,stride ,first-index)
                                 first-index))
                (offset-gensym (if offset
                                   (gensym (symbol-name '#:offset))
                                   nil)))
            (values binding
                    (cond (row-of
                           (let ((accessor (case= row-of
                                             (1 'aref)
                                             (2 'array-row-of-2)
                                             (3 `array-row-of-3)
                                             (4 `array-row-of-4))))
                             `(,accessor ,slot-name ,first-index ,(if offset offset-gensym 0))))
                          (range
                           (let ((accessor (case= range
                                             (1 'aref)
                                             (2 'array-of-2)
                                             (3 `array-of-3)
                                             (4 `array-of-4))))
                             (if offset
                                 `(,accessor ,slot-name (+ ,offset-gensym ,first-index))
                                 `(,accessor ,slot-name ,first-index))))
                          (elt
                           (if offset
                               `(aref ,slot-name ,first-index (+ ,offset-gensym ,elt))
                               `(aref ,slot-name ,first-index ,elt)))
                          (offset
                           `(aref ,slot-name (+ ,offset-gensym ,first-index)))
                          (t
                           `(aref ,slot-name ,first-index)))
                    =
                    (if offset `(,offset-gensym ,offset) nil))))
        (values (prefix-symbol "%" slot)
                (gethash slot slot-gensyms)
                nil
                nil))))

(defmacro with-array-accessors (slots first-index &body body)
  "
Creates symbol macrolets for accessors (like `with-accessors') for
arrays whose first index is in common. Uses `aref' for accessing a
single item from a 1D or 2D array. Otherwise, it uses the special
multiple-value accessors defined earlier in the file, up to 4 values
at a time. When working with multiple values, the symbol accessor will
get and set `values'.

Each slot can be `:row-of' (for true a 2D array), `:range' (for a 1D
array that behaves like a 2D array), or `:elt' (for one item in a 2D
array). Otherwise it's just a simple 1D `aref'. Any of these can have
an `:offset' that shifts the accessor small amount (useful for OpenGL
vertices, which work on ranges) and a `:stride' (an OpenGL concept
that is essentially the row width of a 1D array that is pretending to
be a 2D array) that offsets the first-index itself.

Finally, each slot can also accept a `:=', which automatically sets
the slot to the one or more specified values at the start of the body.

Rows or ranges cannot directly be worked with in one selection if they
are longer than 4, but `:offset' can be used to use more than one
selection on one row, or on a range that conceptually goes together.

This macro seems complex and perhaps unnecessary, but this greatly
simplifies working with both OpenGL vertex arrays (using the
static-vectors library so they're bilingual arrays) and entity
component systems. Just be aware that most of the generated symbols
that appear to be a single variable will get and set multiple values.
"
  (with-gensyms (i)
    (loop :with slot-gensyms := (make-hash-table)
          :with symbol-macrolet-binding := nil
          :with setter := nil
          :with offset-gensym-binding := nil
          :for slot :in slots
          :for slot-name := (if (listp slot) (nth 1 slot) slot)
          :unless (hash-table-value-present? slot-name slot-gensyms)
            :do (setf (gethash slot-name slot-gensyms) (gensym (symbol-name slot-name)))
            :and :collect `(,(gethash slot-name slot-gensyms) ,slot-name)
                   :into slot-gensym-bindings
          :end
          :do (multiple-value-bind (symbol expansion set-value offset-gensym)
                  (parse-array-accessor i slot slot-gensyms)
                (psetf symbol-macrolet-binding `(,symbol ,expansion)
                       setter (if set-value
                                  `(,symbol ,set-value)
                                  nil)
                       offset-gensym-binding offset-gensym))
          :collect symbol-macrolet-binding :into symbol-macrolet-bindings
          :when setter
            :append setter :into setters
          :when offset-gensym-binding
            :collect offset-gensym-binding :into offset-gensym-bindings
          :finally (return `(let ((,i ,first-index)
                                  ,@offset-gensym-bindings
                                  ,@slot-gensym-bindings)
                              (declare (ignorable ,i))
                              (symbol-macrolet
                                  ,symbol-macrolet-bindings
                                ,@(if setters (list `(psetf ,@setters)) nil)
                                ,@body))))))
