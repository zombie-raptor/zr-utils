(cl:defpackage #:zr-utils/data-structures
  (:documentation "Various useful, simple data structures.")
  (:use #:cl
        #:zr-utils/modify-macros
        #:zr-utils/util)
  (:import-from #:alexandria
                #:with-gensyms)
  (:export #:define-struct-queue
           #:define-typed-list))

(cl:in-package #:zr-utils/data-structures)

;;; Queue (implemented as a circular buffer)
(defmacro define-struct-queue (name ((item-name item-type) item-maximum))
  (with-interned-symbols (queue
                          start
                          end
                          empty?
                          (with-queue    name :prefix #:with- :suffix #:-queue)
                          (typed-dequeue name :suffix #:-dequeue)
                          (typed-enqueue name :suffix #:-enqueue))
    (let ((item-maximum-value (if (and (symbolp item-maximum) (constantp item-maximum))
                                  (symbol-value item-maximum)
                                  item-maximum)))
      `(progn
         (define-simple-struct ,name
           (,queue (simple-array ,item-type (,item-maximum-value)))
           (,start (mod ,item-maximum-value))
           (,end (mod ,item-maximum-value))
           (,empty? boolean))
         (define-accessor-macro ,with-queue
             ,(concatenate 'string (symbol-name name) "-"))
         (define-function ,typed-dequeue ((,name ,name))
           (,with-queue (,queue ,start ,end ,empty?) ,name
             (prog1 (aref ,queue ,start)
               (incf-mod ,start ,item-maximum)
               (when (= ,start ,end)
                 (setf ,empty? t)))))
         (define-function ,typed-enqueue ((,name ,name) (,item-name ,item-type))
           (,with-queue (,queue ,start ,end ,empty?) ,name
             (setf (aref ,queue ,end) ,item-name)
             (incf-mod ,end ,item-maximum)
             (when ,empty?
               (setf ,empty? nil))
             ,item-name))))))

;;; Typed Lists
(defmacro define-typed-list (type &optional default)
  (check-type type symbol)
  (with-interned-symbols ((name               type :suffix #:-cons)
                          (car                type :suffix #:-car)
                          (cdr                type :suffix #:-cdr)
                          (car-keyword        type :suffix #:-car     :package :keyword)
                          (cdr-keyword        type :suffix #:-cdr     :package :keyword)
                          (make-cons          type :prefix #:make-    :suffix #:-cons)
                          (list               type :suffix #:-list)
                          (last               type :suffix #:-last)
                          (length             type :suffix #:-length)
                          (do-typed-list      type :prefix #:do-      :suffix #:-list)
                          (list-to-typed-list type :prefix #:list-to- :suffix #:-list)
                          action)
    `(progn (defstruct (,name (:conc-name nil))
              (,car ,default :type ,type)
              (,cdr nil :type (or null ,name)))
            (deftype ,list () ',name)
            (define-function (,name :inline t) (,car ,cdr)
              (,make-cons ,car-keyword ,car
                          ,cdr-keyword ,cdr))
            (define-function (,list :inline t) (&rest input-list)
              (let* ((result (,name (car input-list) nil))
                     (active-cons result))
                (dolist (item (cdr input-list) result)
                  (setf active-cons (setf (,cdr active-cons) (,name item nil))))))
            (define-function (,last :inline t) ((,list ,list))
              (do* ((sublist ,list (,cdr sublist))
                    (end nil (or sublist end)))
                   ((null sublist) end)))
            (define-function (,list-to-typed-list :inline t) (list ,action)
              (let* ((result (,name (funcall ,action (car list)) nil))
                     (active-cons result))
                (dolist (item (cdr list) result)
                  (setf active-cons (setf (,cdr active-cons) (,name (funcall ,action item) nil))))))
            (defmacro ,do-typed-list ((var ,list &optional result) &body body)
              (with-gensyms (typed-list)
                (let ((typed-car ',car)
                      (typed-cdr ',cdr))
                  `(do ((,typed-list ,,list (,typed-cdr ,typed-list)))
                       ((null ,typed-list) ,result)
                     (let ((,var (,typed-car ,typed-list)))
                       ,@body)))))
            (define-function (,length :inline t) ((,list ,list))
              (let ((length 0))
                (,do-typed-list (,type ,list length)
                  (declare (ignore ,type))
                  (incf length))))
            (defmethod print-object ((cons ,name) stream)
              (let* ((printed-list-prefix (format nil "(~S " ',list))
                     (indentation (make-string (length printed-list-prefix) :initial-element #\Space)))
                (format stream "~A" printed-list-prefix)
                (print-object (,car cons) stream)
                (,do-typed-list (item (,cdr cons))
                  (terpri stream)
                  (write-string indentation stream)
                  (print-object item stream)))
              (write-char #\) stream))
            ;; Note: The following won't work on a circular list.
            (define-simple-slot-saving-for-class ,name))))
