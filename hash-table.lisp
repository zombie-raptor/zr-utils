(cl:defpackage #:zr-utils/hash-table
  (:documentation "Helper functions and macros for working with hash tables.")
  (:use #:cl
        #:zr-utils/conditions
        #:zr-utils/util)
  (:import-from #:alexandria
                #:with-gensyms)
  (:export #:do-hash-table
           #:hash
           #:hash-table-value-present-p
           #:hash-table-value-present?
           #:insert-unique-hash-table-value))

(cl:in-package #:zr-utils/hash-table)

(define-function (hash-table-value-present? :inline t) (key hash-table)
  "
Returns T if a value is present in HASH-TABLE for the given key KEY
and NIL if the value is not present.
"
  (nth-value 1 (gethash key hash-table)))

(define-function (hash-table-value-present-p :inline t) (key hash-table)
  "
Returns T if a value is present in HASH-TABLE for the given key KEY
and NIL if the value is not present.
"
  (nth-value 1 (gethash key hash-table)))

(define-function (insert-unique-hash-table-value :inline t)
    (hash-table key value error-string)
  "
Insert VALUE into HASH-TABLE with a key KEY if there is no value
already at KEY. If there is, then there is an error with the
ERROR-STRING as its message and KEY as its argument.
"
  ;; TODO: replace with custom-defined condition?
  (error-when (hash-table-value-present? key hash-table)
              error-string
              key)
  (setf (gethash key hash-table) value))

(defmacro do-hash-table ((key value hash-table &optional result) &body body)
  "Iterates through a hash table key and value just like in `dolist'."
  (let ((maphash `(maphash (lambda (,key ,value)
                             ,@body)
                           ,hash-table)))
    (if result
        `(progn ,maphash ,result)
        maphash)))

(defmacro hash (&rest forms)
  "
Provides a syntax for hash-table creation, with FORMS as a plist
alternating between a key and a value. For example, this creates a
hash-table with two elements with the first having the key :foo and
the value 42 and the second having the key :bar and the value 2:

  (hash :foo 42 :bar 2)
"
  (with-gensyms (hash-table)
    `(let ((,hash-table (make-hash-table)))
       ;; Note: destructuring-bind is used instead of loop's built-in
       ;; destructuring so that a syntax error can happen.
       (psetf ,@(loop :for sublist :on forms :by #'cddr
                      :append (destructuring-bind (key value &rest rest)
                                  sublist
                                (declare (ignore rest))
                                `((gethash ,key ,hash-table) ,value))))
       ,hash-table)))
