;;;; This is an internal file that parses the "typed bindings", i.e
;;;; lambda-lists and bindings where a type can be provided.
;;;;
;;;; Note: This file is "internal". You probably do not need to use
;;;; what it exports.

(defpackage #:zr-utils/typed-bindings
  (:documentation "Parses typed bindings for macros such as define-function.")
  (:use #:cl)
  (:import-from #:alexandria
                #:make-gensym-list)
  (:import-from #:trivial-with-current-source-form)
  (:export #:generate-bindings
           #:generate-bindings*
           #:generate-flet*-or-labels*
           #:generate-function-definition
           #:parse-typed-lambda-list
           #:return-type-check))

(in-package #:zr-utils/typed-bindings)

;;; Parses a binding in a way that depends on its position in the
;;; typed-lambda-list.
(defun parse-typed-lambda-list-item (item section default-type check-type?)
  (trivial-with-current-source-form:with-current-source-form (item)
    (multiple-value-bind (binding type default new-section)
        (etypecase item
          (list (case section
                  (:first
                   (destructuring-bind (binding type)
                       item
                     (check-type binding symbol)
                     (check-type type (or symbol list))
                     (values binding type)))
                  ((:key :optional)
                   (destructuring-bind (binding &optional default type)
                       item
                     (check-type binding symbol)
                     (check-type type (or null symbol list))
                     (values binding (or type default-type) default)))
                  (t item)))
          (symbol (let ((symbol-name (symbol-name item)))
                    (cond ((char= #\& (schar symbol-name 0))
                           (values item nil nil (intern (subseq symbol-name 1) 'keyword)))
                          ;; The &rest item cannot use the default-type
                          ;; because the &rest item is a list.
                          ((eql section :rest)
                           (values item t))
                          (t
                           (values item default-type))))))
      (let ((binding (if default
                         `(,binding ,default)
                         binding))
            (type (if type
                      (if check-type?
                          `(check-type ,binding ,type)
                          `(type ,type ,binding))
                      nil))
            (full-type (let ((full-type* (cond (type type)
                                               (new-section item)
                                               (t t))))
                         (if (eql section :key)
                             `(,(intern (symbol-name binding) '#:keyword) ,full-type*)
                             full-type*))))
        (values binding
                type
                (or new-section section)
                full-type)))))

;;; Parses a typed-lambda-list, which is similar to the `defmethod'
;;; lambda-list.
(defun parse-typed-lambda-list (typed-lambda-list default-type check-type?)
  (trivial-with-current-source-form:with-current-source-form (typed-lambda-list)
    (loop :with section := :first
          :with binding
          :with type
          :with full-type
          :for item :in typed-lambda-list
          :do (setf (values binding type section full-type)
                    (parse-typed-lambda-list-item item section default-type check-type?))
          :collect binding :into bindings
          :when type
            :collect type :into types
          ;; The list full-type is used by ftype because it must always
          ;; be provided, assuming T if the user did not provide it.
          :collect full-type :into full-types
          :finally (return (values bindings types full-types)))))

;;; Provides several reasonable optimization declarations.
(defun optimization-declarations (optimize debug)
  (cond ((and optimize debug) `((optimize (speed 3) (debug 3))))
        (optimize `((optimize (compilation-speed 1) (debug 1) (safety 1) (space 1) (speed 3))))
        (debug `((optimize (speed 1) (debug 3))))
        (t nil)))

;;; Splits off any potential docstrings or declarations from the body
;;; so that the `check-type's can come before the rest of the body.
(defun split-body (body)
  (flet ((declaration? (thing)
           (and (consp thing)
                (eql (car thing) 'declare))))
    ;; Only try to parse if there's a body.
    (if body
        ;; There are five remaining cases because the DECLARE and the
        ;; docstring are both optional and can come in either order,
        ;; with the caveat that the function body cannot end with a
        ;; docstring (it's just a regular string if it ends with a
        ;; string).
        (destructuring-bind (first &rest rest)
            body
          (cond ((and rest (stringp first))
                 (if (declaration? (first rest))
                     (values `(,first)
                             `(,(first rest))
                             (rest rest))
                     (values `(,first)
                             nil
                             rest)))
                ((declaration? first)
                 (if (and rest
                          (rest rest)
                          (stringp (first rest)))
                     (values `(,(first rest))
                             `(,first)
                             (rest rest))
                     (values nil
                             `(,first)
                             rest)))
                (t
                 (values nil nil body))))
        nil)))

;;; Adds return type `check-type's for a known amount of multiple
;;; return values.
(defun return-type-check (body return)
  (let ((results (make-gensym-list (length return) 'result)))
    `((multiple-value-bind ,results (progn ,@body)
        ,@(mapcar (lambda (symbol value)
                    `(check-type ,symbol ,value))
                  results
                  return)
        (values ,@results)))))

;;; Generates a function body for a typed variations of `defun' and
;;; `flet'.
(defun generate-function-body (body check-type types return optimizations)
  (if check-type
      (multiple-value-bind (docstring
                            declaration
                            body)
          (split-body body)
        (let ((body (if return
                        (return-type-check body return)
                        body)))
          `(,@(if optimizations `((declare ,@optimizations)) nil)
            ,@docstring
            ,@declaration
            ,@types
            ,@body)))
      `((declare ,@types ,@optimizations)
        ,@body)))

;;; Generates the ftype of a function for given input types and return
;;; types if using `check-type' is not desired.
(defun generate-ftype (name types return check-type)
  (if (and return (not check-type))
      `((ftype (function ,types (values ,@return)) ,name))
      nil))

;;; Generates a function definition for a typed variation of `defun'
;;; or `flet' using the above helper functions.
(defun generate-function-definition (name-and-options typed-lambda-list body)
  (destructuring-bind (name &key inline optimize debug default-type check-type return)
      (etypecase name-and-options
        (symbol `(,name-and-options))
        (list (if (eql (car name-and-options) 'setf)
                  `(,name-and-options)
                  name-and-options)))
    (multiple-value-bind (lambda-list types full-types)
        (parse-typed-lambda-list typed-lambda-list default-type check-type)
      (let* ((optimizations (optimization-declarations optimize debug))
             (return-suffix (if check-type nil '(&optional)))
             (return (cond ((null return) return)
                           ((listp return) `(,@return ,@return-suffix))
                           (t `(,return ,@return-suffix))))
             (body (generate-function-body body check-type types return optimizations)))
        (values `(,name ,lambda-list ,@body)
                (if inline `((inline ,name)) nil)
                (generate-ftype name full-types return check-type))))))

;;; Destructures `flet'-style syntax into the arguments that
;;; `generate-function-definition' expects.
(defun generate-function-definition* (typed-definition)
  (destructuring-bind (name-and-options typed-lambda-list &body body)
      typed-definition
    (generate-function-definition name-and-options typed-lambda-list body)))

;;; Produces a variation of `flet' or `labels' with very similar
;;; syntax to define-function.
(defun generate-flet*-or-labels* (macro-name definitions body)
  (multiple-value-bind (function-definitions inlines ftypes)
      (loop :for definition :in definitions
            :for function-definition := nil
            :for inline := nil
            :for ftype := nil
            :do (setf (values function-definition inline ftype)
                      (generate-function-definition* definition))
            :collect function-definition :into function-definitions
            :when inline
              :append inline :into inlines
            :when ftype
              :append ftype :into ftypes
            :finally (return (values function-definitions inlines ftypes)))
    (let ((declarations (if (or inlines ftypes)
                            `((declare ,@inlines ,@ftypes))
                            nil)))
      `(,macro-name ,function-definitions ,@declarations ,@body))))

;;; Parses the typed `let'-style bindings into an untyped
;;; `let'-binding and type information.
(defun parse-typed-binding (first rest)
  (multiple-value-bind (name type binding)
      (destructuring-bind (type &rest binding)
          rest
        (values first
                (if (or (eql type t) (null type))
                    nil
                    type)
                binding))
    (values `(:let ,name ,@binding)
            (if type
                `(:let type ,type ,name)
                nil))))

;;; Parses the typed multiple value binds into an untyped m-v-b style
;;; binding and type information.
(defun parse-multiple-value-typed-binding (list)
  (destructuring-bind (names types &rest binding)
      list
    (check-type names list)
    (check-type types list)
    (assert (<= (length types) (length names)))
    (values (list* :values names binding)
            (list* :values
                   (loop :for name :in names
                         :for type :in types
                         :unless (or (eql type t) (null type))
                           :collect `(type ,type ,name))))))

(defun parse-destructuring-typed-binding (list)
  (destructuring-bind (lambda-list types-for-lambda-list expression)
      list
    (declare (ignore lambda-list types-for-lambda-list expression))
    (values (list* :destructuring)
            (list* :destructuring))))

(defun parse-typed-bindings (bindings)
  (loop :for (first . rest) :in bindings
        :with binding
        :with type
        :with function
        :with label
        :do (setf (values binding type function label)
                  (etypecase first
                    (keyword (ecase first
                               ;; (:options)
                               (:let (parse-typed-binding (first rest) (rest rest)))
                               (:values (parse-multiple-value-typed-binding rest))
                               (:destructuring (parse-destructuring-typed-binding rest))
                               (:function (values nil nil rest nil))
                               (:label (values nil nil nil rest))))
                    (symbol (parse-typed-binding first rest))))
        :when binding
          :collect binding :into processed-bindings
        :when type
          :collect type :into types
        :when function
          :collect function :into functions
        :when label
          :collect label :into labels
        :finally (return (values processed-bindings types functions labels))))

(defun split-bindings (bindings)
  (loop :for (binding-type . binding) :in bindings
        :for multiple-value-bind := nil
        :if (eql binding-type :let)
          :collect binding :into bindings*
        :else
          :if (eql binding-type :values)
            :append (destructuring-bind (values-bindings value-form)
                        binding
                      (let ((gensym-bindings (mapcar (lambda (binding)
                                                       (gensym (symbol-name binding)))
                                                     values-bindings)))
                        (setf multiple-value-bind `(,gensym-bindings ,value-form))
                        (mapcar (lambda (binding gensym-binding)
                                  `(,binding ,gensym-binding))
                                values-bindings
                                gensym-bindings)))
              :into bindings*
        :when multiple-value-bind
          :collect multiple-value-bind :into multiple-value-binds
        :finally (return (values bindings* multiple-value-binds))))

(defun generate-multiple-value-binds (multiple-value-binds body)
  (if multiple-value-binds
      (loop :for (bindings value-form) :in multiple-value-binds
            :for previous-form := nil :then form
            :for form := (list 'multiple-value-bind bindings value-form)
            :for first := form :then first
            :when previous-form
              :do (nconc previous-form (list form))
            :finally (progn
                       (setf (cdr (last form)) (list body))
                       (return first)))
      body))

(defun generate-type-declarations (types)
  (loop :for (binding-type . type) :in types
        :if (eql binding-type :let)
          :collect type :into types*
        :else
          :append type :into types*
        :finally (return (list* 'declare types*))))

(defun generate-bindings (bindings body labels-macro flet-macro let-macro)
  (check-type labels-macro (and (not null) symbol))
  (check-type flet-macro (and (not null) symbol))
  (check-type let-macro (and (not null) symbol))
  (multiple-value-bind (bindings types functions labels)
      (parse-typed-bindings bindings)
    (multiple-value-bind (let-bindings multiple-value-binds) (split-bindings bindings)
      (let* ((body (if labels `((,labels-macro ,labels ,@body)) body))
             (body (if functions `((,flet-macro ,functions ,@body)) body))
             (body (if let-bindings
                       `(,let-macro
                         ,let-bindings
                         ,(generate-type-declarations types)
                         ,@body)
                       (destructuring-bind (first) body
                         first))))
        (generate-multiple-value-binds multiple-value-binds body)))))

(defun generate-bindings* (bindings body labels-macro flet-macro let-macro)
  (loop :for binding :in bindings
        :for (first . rest) := binding
        :for previous-binding-type := nil :then binding-type
        :for binding-type := (if (keywordp first) first :let)
        :with binding-group
        :with bindings* := (list)
        :do (if (and (or (eql previous-binding-type binding-type)
                         (null previous-binding-type))
                     (or (eql binding-type :let)
                         (eql binding-type :label)))
                (setf binding-group (nconc binding-group (list binding)))
                (progn (setf binding-type nil)
                       (when binding-group
                         (setf bindings* (push binding-group bindings*)
                               binding-group nil))
                       (unless (or (eql binding-type :let)
                                   (eql binding-type :label))
                         (setf bindings* (push (list binding) bindings*)))))
        :finally (return (progn
                           (when binding-group
                             (setf bindings* (push binding-group bindings*)))
                           (loop :for binding-group :in bindings*
                                 :for previous := body :then (list result)
                                 :for result := (generate-bindings binding-group
                                                                   previous
                                                                   labels-macro
                                                                   flet-macro
                                                                   let-macro)
                                 :finally (return result))))))
