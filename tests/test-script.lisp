(defpackage #:test-script
  (:use #:cl))

(in-package #:test-script)

;;; Load the test suite and UIOP at read time.
#.(progn (ql:quickload :fiveam :silent t)
         (ql:quickload :uiop :silent t)
         nil)

;;; Turn on full type inference to catch more type errors when
;;; compiling.
#+sbcl
(setf sb-ext:*derive-function-types* t)

;;; Load only the parts required by the test system. Load with verbose
;;; so warnings are visible.
(ql:quickload :zr-utils/tests :verbose t)

(defun run-tests ()
  (fiveam:run! 'zr-utils/tests:zr-utils/tests))

;;; This lets Gitlab CI know that something went wrong.
(unless (run-tests)
  (uiop:quit 1))
