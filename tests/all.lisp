(cl:defpackage #:zr-utils/tests/all
  (:use #:cl
        #:zr-utils)
  (:import-from #:alexandria
                #:with-gensyms)
  (:import-from #:fiveam
                #:def-suite
                #:in-suite
                #:is
                #:test)
  (:export #:macroexpansion-equal
           #:zr-utils/tests
           #:zr-utils/tests/define-function))

(cl:in-package #:zr-utils/tests/all)

(def-suite zr-utils/tests)

;;; Note: For these tests, a lot of the names are not particularly
;;; idiomatic because the most idiomatic thing to do would be to have
;;; the variable and type name be the same. However, that would
;;; prevent some errors in the macro expansion from being identified.

(defun %macroexpansion-equal (actual-form expected-form cache)
  "Recursive helper function for `macroexpansion-equal'."
  (typecase actual-form
    (list (and (= (length actual-form) (length expected-form))
               (every (lambda (x y)
                        (%macroexpansion-equal x y cache))
                      actual-form expected-form)))
    (symbol (cond ((symbol-package actual-form)
                   (equal expected-form actual-form))
                  ((and (symbolp expected-form)
                        (not (symbol-package actual-form)))
                   (multiple-value-bind (cached-form presentp)
                       (gethash actual-form cache)
                     (if presentp
                         (equal expected-form cached-form)
                         (progn
                           (setf (gethash actual-form cache)
                                 expected-form)
                           t))))
                  (t nil)))
    (t (equal actual-form expected-form))))

(defun macroexpansion-equal (actual-form expected-form)
  "
Tests if a macro-expanded form is equal to its expected form. The only
difference with this and `equal' is that it has some logic to handle
symbols produced by `gensym'. This expects the actual-form to be
macro-expanded, probably via `macroexpand-1'.

This doesn't actually do the macro expansion because some other macro
expansion function might have been used and because both the input and
the macro expansion should be visible to the test suite.

The only special case is gensyms. If it encounters a gensym (a symbol
not part of a package), then the first time, any symbol not part of a
package counts for the sake of this equality test. Any other time, it
uses a hash table cache to make sure that both macros are using the
\"same\" gensyms in the same places.

This only needs to be used instead of `equal' if gensymed symbols are
expected in the macro expansion.
"
  (%macroexpansion-equal actual-form expected-form (make-hash-table)))

(def-suite zr-utils/tests/define-function
  :in zr-utils/tests)

(in-suite zr-utils/tests/define-function)

(test empty-functions
  (is (equal
       `(defun foo ()
          (declare))
       (macroexpand-1
        `(define-function foo ())))
      "Does the simplest empty function generate an empty function?")

  (is (equal
       `(defun foo ())
       (macroexpand-1
        `(define-function (foo :check-type t) ())))
      "Does the empty check-type function generate an empty function?"))

(test hello-world
  (is (equal
       `(defun hello-world ()
          (declare)
          (print "Hello world!"))
       (macroexpand-1
        `(define-function hello-world ()
           (print "Hello world!"))))
      "Does define-function work as expected for a simple hello world function?")

  (is (equal
       `(progn (declaim (inline hello-world))
               (defun hello-world ()
                 (declare)
                 (print "Hello world!")))
       (macroexpand-1
        `(define-function (hello-world :inline t) ()
           (print "Hello world!"))))
      "Does define-function work as expected for a simple inline function?"))

(test docstrings
  (is (equal
       `(defun hello-world ()
          (declare)
          "A simple hello world."
          (print "Hello world!"))
       (macroexpand-1
        `(define-function hello-world ()
           "A simple hello world."
           (print "Hello world!"))))
      "Does define-function preserve the docstring in a simple hello world function?")

  (is (macroexpansion-equal
       (with-gensyms (result)
         `(defun be-careful ()
            (multiple-value-bind (,result)
                (progn "This is not a docstring!")
              (check-type ,result string)
              (values ,result))))
       (macroexpand-1
        `(define-function (be-careful :check-type t :return string) ()
           "This is not a docstring!")))
      "
Does define-function treat a one-liner function returning a string
correctly instead of parsing it as a docstring? This uses a few
additions to ensure that the docstring and not docstring
macroexpansion would be different.
")

  (is (macroexpansion-equal
       (with-gensyms (result)
         `(defun be-careful ()
            "This is a docstring!"
            (multiple-value-bind (,result)
                (progn "This is not a docstring!")
              (check-type ,result string)
              (values ,result))))
       (macroexpand-1
        `(define-function (be-careful :check-type t :return string) ()
           "This is a docstring!"
           "This is not a docstring!")))
      "Does return type checking place the docstring in the correct location?")

  (is (equal
       `(defun foo (x)
          "Hello"
          (declare (optimize (speed 3)))
          (check-type x fixnum)
          x)
       (macroexpand-1
        `(define-function (foo :check-type t) ((x fixnum))
           "Hello"
           (declare (optimize (speed 3)))
           x)))
      "Does type checking work if there's a docstring followed by a declaration?")

  (is (equal
       `(defun foo (x)
          "Hello"
          (declare (optimize (speed 3)))
          (check-type x fixnum)
          x)
       (macroexpand-1
        `(define-function (foo :check-type t) ((x fixnum))
           (declare (optimize (speed 3)))
           "Hello"
           x)))
      "Does type checking work if there's a declaration followed by a docstring?"))

(test simple-typed-input
  (is (equal
       `(defun hello (p)
          (declare (type person p))
          (format t "Hello, ~A!" p))
       (macroexpand-1
        `(define-function hello ((p person))
           (format t "Hello, ~A!" p))))
      "Does define-function work as expected with a type declaration?")

  (is (equal
       `(defun hello (p)
          (check-type p person)
          (format t "Hello, ~A!" p))
       (macroexpand-1
        `(define-function (hello :check-type t) ((p person))
           (format t "Hello, ~A!" p))))
      "Does define-function work as expected with a generated check-type?"))

(test multiple-input-types-with-no-return-types
  (is (equal
       `(defun add (x y)
          (declare (type integer x)
                   (type complex y))
          (+ x y))
       (macroexpand-1
        `(define-function add ((x integer) (y complex))
           (+ x y)))))

  (is (equal
       `(defun add (x y)
          (check-type x integer)
          (check-type y complex)
          (+ x y))
       (macroexpand-1
        `(define-function (add :check-type t) ((x integer) (y complex))
           (+ x y))))))

(test define-function-documentation-examples
  (is (equal
       `(defun foo (x y &optional (z 42))
          (declare (type single-float x)
                   (type double-float y)
                   (type fixnum z))
          (+ x y z))
       (macroexpand-1
        `(define-function foo ((x single-float) (y double-float) &optional (z 42 fixnum))
           (+ x y z))))
      "Does the first example from the define-function docstring still work?")

  (is (equal
       `(progn (declaim (inline bar)
                        (ftype (function (integer single-float)
                                         (values single-float &optional))
                               bar))
               (defun bar (x y)
                 (declare (type integer x)
                          (type single-float y))
                 (* x y)))
       (macroexpand-1
        `(define-function (bar :inline t :return single-float) ((x integer) (y single-float))
           (* x y))))
      "Does the second example from the define-function docstring still work?"))
