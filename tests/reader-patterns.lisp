(cl:defpackage #:zr-utils/tests/reader-patterns
  (:use #:cl
        #:zr-utils/reader-patterns
        #:zr-utils/tests/all)
  (:export #:test-reader-patterns))

(cl:in-package #:zr-utils/tests/reader-patterns)
